import { Recipe } from '../recipe.model';
import {
  ADD_RECIPE,
  DELETE_RECIPE,
  RecipeActions,
  SET_RECIPES,
  UPDATE_RECIPE,
} from './recipe.actions';

export interface State {
  recipes: Recipe[];
}

const intialState: State = {
  recipes: [],
};

export function recipeReducer(
  state: State = intialState,
  action: RecipeActions
) {
  switch (action.type) {
    case SET_RECIPES:
      return {
        ...state,
        recipes: [...action.payload],
      };
    case ADD_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, action.payload],
      };
    case DELETE_RECIPE:
      const recipesDeleted = [...state.recipes];
      recipesDeleted.splice(action.payload, 1);
      return {
        ...state,
        recipes: recipesDeleted,
      };
    case UPDATE_RECIPE:
      const recipesUpdated = [...state.recipes];
      recipesUpdated[action.payload.id] = {
        ...state.recipes[action.payload.id],
        ...action.payload.recipe,
      };
      return {
        ...state,
        recipes: recipesUpdated,
      };

    default:
      return state;
  }
}
