import { Directive, ElementRef, Renderer2, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  // isOpen = false;
  @HostBinding('class.open') isOpen = false;
  // constructor(private renderer: Renderer2, private elementRef: ElementRef) { }
  // constructor() {}
  constructor(private elementRef: ElementRef) {}

  // @HostListener('click')
  // toggleOpen() {
  //   this.isOpen = !this.isOpen;
  //   // if (this.isOpen) {
  //   //   this.renderer.addClass(this.elementRef.nativeElement, 'open');
  //   // } else {
  //   //   this.renderer.removeClass(this.elementRef.nativeElement, 'open');
  //   // }
  // }

  @HostListener('document:click', ['$event']) toggleOnDocument(event: Event) {
    this.isOpen = this.elementRef.nativeElement.contains(event.target) ? !this.isOpen : false;
  }

}
