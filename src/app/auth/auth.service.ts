import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Logout } from './store/auth.actions';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  expirationTimer: any;

  constructor(private store: Store) {}

  clearLogoutTimer() {
    if (this.expirationTimer) {
      clearTimeout(this.expirationTimer);
    }
    this.expirationTimer = null;
  }

  setLogoutTimer(expirationTime: number) {
    this.expirationTimer = setTimeout(() => {
      this.store.dispatch(new Logout());
    }, expirationTime);
  }
}
