import { User } from '../user.model';
import * as AuthActions from './auth.actions';
import { AuthEffects } from './auth.effects';

export interface State {
  user: User;
  authErr: string;
  isLoading: boolean;
}

const initialState: State = {
  user: null,
  authErr: null,
  isLoading: false
};

export function authReducer(state: State = initialState, action: AuthActions.AuthActions) {
  console.log(action.type);
  console.log(state);
  switch (action.type) {
    case AuthActions.AUTHENTICATION_SUCCESS:
      const user = new User(
        action.payload.email,
        action.payload.id,
        action.payload.token,
        action.payload.expirationDate
      );
      return {
        ...state,
        user,
        authErr: null,
        isLoading: false
      };
    case AuthActions.LOGOUT:
      return {
        ...state,
        user: null,
      };
    case AuthActions.LOGIN_START:
    case AuthActions.SIGNUP_START:
      return {
        ...state,
        authErr: null,
        isLoading: true
      };
    case AuthActions.AUTHENTICATION_FAIL:
      return {
        ...state,
        authErr: action.payload,
        isLoading: false
      };
    case AuthActions.CLEAR_ERROR:
      return {
        ...state,
        authErr: null,
      };
    default:
      return state;
  }
}
